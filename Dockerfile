#FROM python
#WORKDIR /app
#COPY . /app
#CMD ["python3","app.py"]

#FROM nginx
#COPY myText.html /usr/share/nginx/html

FROM python:3
WORKDIR /app
COPY . /app
CMD ["python3","-m","http.server","8181"]
